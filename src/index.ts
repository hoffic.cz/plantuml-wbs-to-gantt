import {parseWbsText} from "./parser";
import {populate} from "./populator";
import {outputGanttText} from "./types/outputter";
import {formatDate} from "./util";

export const wbsToGantt = (wbsText: string): string => {
    const tree = parseWbsText(wbsText);
    populate(tree);
    return outputGanttText(tree, [
        `project starts ${formatDate()}`,
        'projectscale weekly',
    ]);
}

const input = `
@startwbs
* We Predict Web System
** Development
*** Discovery
**** Discussions with the client
**** Investigate data sources
**** Explore the NHTSA data set
**** POC with NHTSA data
*** Set up a project skeleton
**** Configure docker runtime image
**** Configure Symfony framework
**** Configure Sonata Admin bundle
**** Configure database
*** Import the NHTSA data set
**** Explore the structure
**** Create a parser
**** Deal with corrupted entries
*** Create user interface
**** Configure database mapping
**** Configure data filters
*** Create data insights
**** Set up SQL query pipe translation
**** Implement statistical functions
**** Visualize the selected data
*** Expand to other tables
** Evaluation and Handover
*** Evaluation
**** Usability testing
**** Acceptance testing
*** Handover
**** Create user documentation
**** Create dev documentation
**** Create installation means
@endwbs
`;

console.log(wbsToGantt(input));
