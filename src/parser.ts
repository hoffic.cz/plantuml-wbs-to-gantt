import {WbsNode} from "./types/WbsNode";

export const parseWbsText = (wbsText: string) => {
    const lines = wbsText.trim().split('\n');

    validate(lines);

    const root: WbsNode = new WbsNode(getName(lines.shift() as string));
    const levels: WbsNode[] = [root];

    for (const line of lines) {
        const level = getLevel(line);

        if (level > levels.length) {
            const node = new WbsNode(getName(line));
            levels[levels.length - 1].children.push(node);
            levels.push(node);

        } else if (level === levels.length) {
            const node = new WbsNode(getName(line));
            levels.pop();
            levels[levels.length - 1].children.push(node);
            levels.push(node);

        } else {
            const node = new WbsNode(getName(line));
            for (let i = levels.length - level + 1; i > 0; i--) {
                levels.pop();
            }
            levels[levels.length - 1].children.push(node);
            levels.push(node);
        }
    }

    return root;
}

const getLevel = (line: string): number => {
    return line.split(' ')[0].length;
}

const getName = (line: string): string => {
    return line.split(' ').slice(1).join(' ');
}

const validate = (lines: string[]) => {
    if (lines[0] === '@startwbs') {
        lines.shift();
    } else {
        throw new Error('The first line must be @startwbs.');
    }
    if (lines[lines.length - 1] === '@endwbs') {
        lines.pop();
    } else {
        throw new Error('The last line must be @endwbs.');
    }
    for (const line of lines) {
        validateLine(line);
    }
    if (lines.length <= 0) {
        throw new Error('At least one node is required.');
    }
}

const validateLine = (line: string) => {
    if (!line.startsWith('*')) {
        throw new Error('Everything between @startwbs and @endwbs must start with *.');
    }
    if (line.includes('[') || line.includes(']')) {
        throw new Error('Square brackets must not be used.');
    }
}
