import {WbsNode} from "./types/WbsNode";

export const populate = (tree: WbsNode): number => {
    let depth = tree.children.length === 0
        ? 1
        : tree.children
            .map(subtree => populate(subtree))
            .reduce((acc, curr) => acc + curr, 0);

    tree.depth = depth;

    return depth;
}
