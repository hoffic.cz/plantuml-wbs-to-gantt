export class WbsNode {
    name: string;
    children: WbsNode[];
    depth?: number;

    constructor(name: string) {
        this.name = name;
        this.children = [];
    }
}
