import {WbsNode} from "./WbsNode";

const INDENTATION = 4;
const MILESTONE_DEPTH = 1;

export const outputGanttText = (tree: WbsNode, customConfig: string[] = []): string => {
    const builder: string[] = [];
    builder.push('@startgantt');

    builder.push(...customConfig);
    builder.push('');

    expand(tree, builder);

    builder.push('@endgantt');
    return builder.join('\n');
}

const expand = (tree: WbsNode, builder: string[], dependency?: string, depth: number = -1) => {
    if (depth >= 0) {
        const startCmd = dependency === undefined ? '' : ` starts at [${dependency}]'s end and`;
        builder.push(`${' '.repeat(depth * INDENTATION)}[${tree.name}]${startCmd} lasts ${tree.depth} weeks`);
    }

    let lastDependency = dependency;
    for (const subtree of tree.children) {
        expand(subtree, builder, lastDependency, depth + 1);
        lastDependency = subtree.name;
    }

    if (depth === MILESTONE_DEPTH) {
        builder.push(`${' '.repeat(depth * INDENTATION)}[${tree.name} finished] happens at [${lastDependency}]'s end`);
    }
}
